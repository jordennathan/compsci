/*
 * 
 * Creates the electron window.
 * All written by Nathan Jorden.
 * 
*/

const electron = require('electron');
const url = require('url');
const path = require('path');

const {app, BrowserWindow, Menu, ipcMain} = electron;

require("electron-reload")(__dirname, {
    electron: require('${__dirname}/../../node_modules/electron')
})


let mainWindow;

//Listen for app to be ready.
app.on('ready', function(){
    
    //Create new window.
    mainWindow = new BrowserWindow({
        title: 'Restaurant Management System',
        mazimise: true
    });
    //Load HTML into window.
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, '/app_assets/html/login.html'),
        protocol: 'file:',
        slashes: true
    }));
    
    //Closes whole app when main window is closed.
    mainWindow.on('closed', function(){
        app.quit();
    });
    //Build main menu from template
    const loginMenu = Menu.buildFromTemplate(loginMenuTemplate);
    //Insert into electron
    Menu.setApplicationMenu(loginMenu);
    mainWindow.maximize();
    icon: path.join(__dirname, '/ico/icon.ico')
});



const loginMenuTemplate = [
    {
        label: 'File',
       submenu: [
           {
               label: 'Logout',
               click(){
                mainWindow.loadURL(url.format({
                    pathname: path.join(__dirname, '/app_assets/html/login.html'),
                    protocol: 'file:',
                    slashes: true
                }));
               }
           },
           {type: 'separator'},
           {
               label: 'Quit',
               click(){
                   app.quit();
               }
           }
       ]
    }
    
];

/*
 *
 * 
 * 
*/

if(process.env.NODE_ENV !== 'production'){
    /*require('electron-reload')(__dirname, {
        electron: path.join(__dirname, 'node_modules', '.bin', 'electron') 
    });*/
    loginMenuTemplate.push({

        label: 'Dev Tools',
        submenu: [
            {
                accelerator: 'F5',
                role: 'reload'
            },
            {
                //Adds a hotkey, checks to see if on OSX to change Ctrl to Command.
                accelerator: process.platform == 'darwin' ? 'Command+F5' : 'Ctrl+F5',
                role: 'forcereload'
            },
            {
                accelerator: 'F6',
                role: 'toggledevtools'
            }
        ]
    });
};

//Closes app on exit button pressed.

ipcMain.on('close-please', (e, arg) => {
    console.log(arg);
    app.quit();
    
});
