//Gets electron
const {ipcRenderer} = require('electron')
//Function that communicates with main.js when exit button is pressed.
function login_close(){
    console.log(ipcRenderer.sendSync('close-please', 'ping')) // prints "pong"
}

var users = {1111:
    {pin:1111,
    name: 'Nathan',
    admin: true,
    disabled: false},
    1000:
    {pin:1000,
    name: 'Matthew',
    admin: false,
    disabled: false},
    6969:
    {pin:6969,
    name: 'James',
    admin: true,
    disabled: true}
};
var c1Filepath = "app_assets/txt/c1.txt";// you need to save the filepath when you open the file to update without use the filechooser dialog againg

var c1 = {
};

var itemList = {}
var total;
var uiTotal;
var nam;
var admi;
var pinEnt;
var pinNo;

function logout(){
  window.location.replace('../html/login.html');
};

function checkTime(i) {
    if (i < 10) {
      i = "0" + i;
    }
    return i;
  }
  
  function startTime() {
    var today = new Date();
    var date = today.getDate();
    var month = today.getMonth() + 1;
    var year = today.getFullYear();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    // add a zero in front of numbers<10
    date = checkTime(date);
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('date').innerHTML = date + '/' + month + '/' + year;
    document.getElementById('time').innerHTML = h + ":" + m + ":" + s;
    t = setTimeout(function() {
      startTime()
    }, 500);
  }
  